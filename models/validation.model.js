const Joi = require('joi');

/**
 * Validate product
 * @param {*} params 
 * @returns 
 */
const validateProduct = (params) => {
  const JoiSchema = Joi.object({
    name: Joi.string()
          .min(5)
          .max(50)
          .required()
          .error(() => {
            return { message: 'Please enter a valid product name between 5 to 50 characters.' };
          }),
    price: Joi.number()
          .precision(0)
          .required()
          .error(() => {
            return { message: 'Please enter a valid product price.' };
          }),
    description: Joi.string()
          .optional()
          .allow(null, '')
  }).options({ abortEarly: false });
  const response = JoiSchema.validate(params)
  return response.error ?  
          { error: response.error, code: 400,  message: response.error.details[0].message } 
          : 
          { error:false, code: 200, message: 'OK' }
}

/**
 * Validate product and currency ID
 * @param {*} params 
 * @returns 
 */
const validateProductandCurrencyID = (params) => {
  const JoiSchema = Joi.object({
    id: Joi.number()
          .precision(0)
          .required()
          .error(() => {
            return { message: 'Please enter a valid product ID.' };
          }),
    currency: Joi.string()
          .required()
          .valid('', 'USD', 'EUR', 'CAD', 'GBP')
          .error(() => {
            return { message: 'Please enter a valid currency code.' };
          })
  }).options({ abortEarly: false });
  const response = JoiSchema.validate(params)
  return response.error ?  
          { error: response.error, code: 400,  message: response.error.details[0].message } 
          : 
          { error:false, code: 200, message: 'OK' }
}

/**
 * Validate product ID
 * @param {*} params 
 * @returns 
 */
const validateProductID = (params) => {
  const JoiSchema = Joi.object({
    id: Joi.number()
          .precision(0)
          .required()
          .error(() => {
            return { message: 'Please enter a valid product ID.' };
          })
  }).options({ abortEarly: false });
  const response = JoiSchema.validate(params)
  return response.error ?  
          { error: response.error, code: 400,  message: response.error.details[0].message } 
          : 
          { error:false, code: 200, message: 'OK' }
}

module.exports = { 
    validateProduct, validateProductID, validateProductandCurrencyID
}