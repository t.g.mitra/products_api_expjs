const  express = require('express');
const router = express.Router();
const api = require('../controllers/api.controllers');

router.post("/product/",        api.addProduct);            // Create a new product
router.get("/product/:id",      api.getProduct);            // Get a single product
router.get("/mostviewed",       api.getMostViewed);         // List the most viewed products
router.delete("/product/:id",   api.deleteProduct);         // Delete a product
router.get("/currency",         api.updateCurrencyStatus);  // Get updated currency value

module.exports = router;