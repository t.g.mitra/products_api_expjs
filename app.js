const home = require('./routes/home.routes');
const api_path = require('./routes/api.routes');
const cors = require('cors');
const express = require('express');
const app = express();

require('./startup/prod')(app);

// add cors
app.use(cors());
app.options('*', cors());

// Load body parser
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Load routers
app.use('/', home);
app.use('/api', api_path);
app.use('/static', express.static('public'))

const port = process.env.PORT || 8888;
app.listen(port, () => console.log(`Listning on port ${port}.....`));
app.timeout = 0; //Set to 0 to disable any kind of automatic timeout behavior on incoming connections..

module.exports = app;